var express = require('express');
const merge = require('easy-pdf-merge');

var app = express();

app.get('/', function (req, res) {
  // source file paths
  var files = []
  message = ''
  merge(files, '/output.pdf', function(err){
    if(err) {
      return message = err
    }
    message = 'Successfully merged'
  });
  res.send(message);
});

app.listen(3000, function () {
  console.log('Example app listening on port 3000!');
});
